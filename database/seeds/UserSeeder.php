<?php

use Illuminate\Database\Seeder;
use App\Model\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id' => 1,
            'name' => 'super admin',
            'username' => 'superadmin',
            'email' => 'superadmin@a.com',
            'password' => '$2a$10$Zn4nGOSyacRYiTMRH77R4uX1fiCeeTeTEdsK/y7uWByCTC6u8rQsy'
        ]);


    }
}
