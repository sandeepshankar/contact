<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddressBook extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_book', function(Blueprint $table) {
           $table->increments('address_book_id');
            $table->integer('user_id');
            $table->string('contact_person_name');
            $table->string('contact_person_number');
            $table->string('address_line_1_from');
            $table->string('address_line_2_from')->nullable();
            $table->string('address_line_3_from')->nullable();
            $table->integer('pincode_from');
            $table->string('city_from');
            $table->string('state_from');
            $table->string('country_from');
            $table->string('address_line_1_to');
            $table->string('address_line_2_to')->nullable();
            $table->string('address_line_3_to')->nullable();
            $table->integer('pincode_to');
            $table->string('city_to');
            $table->string('state_to');
            $table->string('country_to');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('address_book');
    }
}
