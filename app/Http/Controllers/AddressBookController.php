<?php

namespace App\Http\Controllers;

use App\Model\AddressBook;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class AddressBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = AddressBook::where('user_id', Auth::user()->id)->paginate(10);
        return View::make('manage_address_book')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('manage_address_book_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'contact_person_name' => 'required|alpha_dash',
            'contact_person_number' => 'required',
            'address_line_1_from' => 'required',
            'address_line_2_from' => 'required',
            'address_line_3_from' => 'required',
            'pincode_from' => 'required',
            'city_from' => 'required',
            'state_from' => 'required',
            'country_from' => 'required',
            'address_line_1_to' => 'required',
            'address_line_2_to' => 'required',
            'address_line_3_to' => 'required',
            'pincode_to' => 'required',
            'city_to' => 'required',
            'state_to' => 'required',
            'country_to' => 'required'
        ]);

        $data = AddressBook::create([
            'user_id' => Auth::user()->id,
            'contact_person_name' => $request->input('contact_person_name'),
            'contact_person_number' => $request->input('contact_person_number'),
            'address_line_1_from' => $request->input('address_line_1_from'),
            'address_line_2_from' => $request->input('address_line_2_from'),
            'address_line_3_from' => $request->input('address_line_3_from'),
            'pincode_from' => $request->input('pincode_from'),
            'city_from' => $request->input('city_from'),
            'state_from' => $request->input('state_from'),
            'country_from' => $request->input('country_from'),
            'address_line_1_to' => $request->input('address_line_1_to'),
            'address_line_2_to' => $request->input('address_line_2_to'),
            'address_line_3_to' => $request->input('address_line_3_to'),
            'pincode_to' => $request->input('pincode_to'),
            'city_to' => $request->input('city_to'),
            'state_to' => $request->input('state_to'),
            'country_to' => $request->input('country_to')
        ]);
        if(($data) && ($data->address_book_id))
        {
            return redirect('/address_book')->with('success', Lang::get('custom.contact_add_success'));
        }
        return redirect('/address_book')->with('error',Lang::get('custom.contact_add_error'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = AddressBook::where('user_id', Auth::user()->id)->where('address_book_id', $id)->first();
        if(($data) && ($data->address_book_id))
        {
            return View::make('manage_address_book_show')->with('data', $data);
        }
        return redirect('/address_book')->with('error','DB Error');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = AddressBook::where('user_id', Auth::user()->id)->where('address_book_id', $id)->first();
        if(($data) && ($data->address_book_id))
        {
            return View::make('manage_address_book_edit')->with('data', $data);
        }
        return redirect('/address_book')->with('error','DB Error');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'address_book_id' => 'required',
            'contact_person_name' => 'required|alpha_dash',
            'contact_person_number' => 'required',
            'address_line_1_from' => 'required',
            'address_line_2_from' => 'required',
            'address_line_3_from' => 'required',
            'pincode_from' => 'required',
            'city_from' => 'required',
            'state_from' => 'required',
            'country_from' => 'required',
            'address_line_1_to' => 'required',
            'address_line_2_to' => 'required',
            'address_line_3_to' => 'required',
            'pincode_to' => 'required',
            'city_to' => 'required',
            'state_to' => 'required',
            'country_to' => 'required'
        ]);

        $data = AddressBook::where('address_book_id', $request->input('address_book_id'))->update([
            'user_id' => Auth::user()->id,
            'contact_person_name' => $request->input('contact_person_name'),
            'contact_person_number' => $request->input('contact_person_number'),
            'address_line_1_from' => $request->input('address_line_1_from'),
            'address_line_2_from' => $request->input('address_line_2_from'),
            'address_line_3_from' => $request->input('address_line_3_from'),
            'pincode_from' => $request->input('pincode_from'),
            'city_from' => $request->input('city_from'),
            'state_from' => $request->input('state_from'),
            'country_from' => $request->input('country_from'),
            'address_line_1_to' => $request->input('address_line_1_to'),
            'address_line_2_to' => $request->input('address_line_2_to'),
            'address_line_3_to' => $request->input('address_line_3_to'),
            'pincode_to' => $request->input('pincode_to'),
            'city_to' => $request->input('city_to'),
            'state_to' => $request->input('state_to'),
            'country_to' => $request->input('country_to')
        ]);
        if($data)
        {
            return redirect('/address_book')->with('success',Lang::get('custom.contact_update_success'));
        }
        return redirect('/address_book')->with('error',Lang::get('custom.contact_update_error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = AddressBook::where('address_book_id', $id)->delete();
        if($data)
        {
            return redirect('/address_book')->with('success',Lang::get('custom.contact_delete_success'));
        }
        return redirect('/address_book')->with('error',Lang::get('custom.contact_delete_error'));
    }
}
