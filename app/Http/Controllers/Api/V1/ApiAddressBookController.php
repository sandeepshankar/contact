<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Model\AddressBook;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\View;

class ApiAddressBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = AddressBook::get()->toJson();
        if ($data) {
            return response()->json(['status' => '1', 'message' => Lang::get('custom.success'), 'data' => $data]);
        }
        return response()->json(['status' => '0', 'message' => Lang::get('custom.db_error')]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'contact_person_name' => 'required|alpha_dash',
            'contact_person_number' => 'required',
            'address_line_1_from' => 'required',
            'address_line_2_from' => 'required',
            'address_line_3_from' => 'required',
            'pincode_from' => 'required',
            'city_from' => 'required',
            'state_from' => 'required',
            'country_from' => 'required',
            'address_line_1_to' => 'required',
            'address_line_2_to' => 'required',
            'address_line_3_to' => 'required',
            'pincode_to' => 'required',
            'city_to' => 'required',
            'state_to' => 'required',
            'country_to' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['status' => '0','message' => $validator->messages()->all()]);
        }

        $data = AddressBook::create([
            'user_id' => 1,
            'contact_person_name' => $request->input('contact_person_name'),
            'contact_person_number' => $request->input('contact_person_number'),
            'address_line_1_from' => $request->input('address_line_1_from'),
            'address_line_2_from' => $request->input('address_line_2_from'),
            'address_line_3_from' => $request->input('address_line_3_from'),
            'pincode_from' => $request->input('pincode_from'),
            'city_from' => $request->input('city_from'),
            'state_from' => $request->input('state_from'),
            'country_from' => $request->input('country_from'),
            'address_line_1_to' => $request->input('address_line_1_to'),
            'address_line_2_to' => $request->input('address_line_2_to'),
            'address_line_3_to' => $request->input('address_line_3_to'),
            'pincode_to' => $request->input('pincode_to'),
            'city_to' => $request->input('city_to'),
            'state_to' => $request->input('state_to'),
            'country_to' => $request->input('country_to')
        ]);
        if($data)
        {
            return response()->json(['status' => '1', 'message' => Lang::get('custom.contact_add_success'), 'data' => $data]);
        }
        return response()->json(['status' => '0', 'message' => Lang::get('custom.contact_add_error')]);
    }

    public function showedit(Request $request)
    {
        $rules = array(
            'address_book_id' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['status' => '0','message' => $validator->messages()->all()]);
        }
        $data = AddressBook::where('user_id', Auth::user()->id)->where('address_book_id', $request->input('address_book_id'))->first();
        if($data)
        {
            return response()->json(['status' => '1', 'message' => Lang::get('custom.success'), 'data' => $data]);
        }
        return response()->json(['status' => '0', 'message' => Lang::get('custom.db_error')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = array(
            'address_book_id' => 'required',
            'contact_person_name' => 'required|alpha_dash',
            'contact_person_number' => 'required',
            'address_line_1_from' => 'required',
            'address_line_2_from' => 'required',
            'address_line_3_from' => 'required',
            'pincode_from' => 'required',
            'city_from' => 'required',
            'state_from' => 'required',
            'country_from' => 'required',
            'address_line_1_to' => 'required',
            'address_line_2_to' => 'required',
            'address_line_3_to' => 'required',
            'pincode_to' => 'required',
            'city_to' => 'required',
            'state_to' => 'required',
            'country_to' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['status' => '0','message' => $validator->messages()->all()]);
        }

        $data = AddressBook::where('address_book_id', $request->input('address_book_id'))->update([
            'user_id' => Auth::user()->id,
            'contact_person_name' => $request->input('contact_person_name'),
            'contact_person_number' => $request->input('contact_person_number'),
            'address_line_1_from' => $request->input('address_line_1_from'),
            'address_line_2_from' => $request->input('address_line_2_from'),
            'address_line_3_from' => $request->input('address_line_3_from'),
            'pincode_from' => $request->input('pincode_from'),
            'city_from' => $request->input('city_from'),
            'state_from' => $request->input('state_from'),
            'country_from' => $request->input('country_from'),
            'address_line_1_to' => $request->input('address_line_1_to'),
            'address_line_2_to' => $request->input('address_line_2_to'),
            'address_line_3_to' => $request->input('address_line_3_to'),
            'pincode_to' => $request->input('pincode_to'),
            'city_to' => $request->input('city_to'),
            'state_to' => $request->input('state_to'),
            'country_to' => $request->input('country_to')
        ]);
        if($data)
        {
            return response()->json(['status' => '1', 'message' => Lang::get('custom.contact_update_success'), 'data' => $data]);
        }
        return response()->json(['status' => '0', 'message' => Lang::get('custom.contact_update_error')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = array(
            'address_book_id' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['status' => '0','message' => $validator->messages()->all()]);
        }
        $data = AddressBook::where('address_book_id', $request->input('address_book_id'))->delete();
        if($data)
        {
            return response()->json(['status' => '1', 'message' => Lang::get('custom.contact_delete_success'), 'data' => $data]);
        }
        return response()->json(['status' => '0', 'message' => Lang::get('custom.contact_delete_error')]);
    }

}
