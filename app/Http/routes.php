<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middlewareGroups' => 'web'], function () {
    Route::get('/', function () { return view('auth/login'); });

    Route::auth();

    // Protected Routes
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/home', 'HomeController@index');

        Route::get('/address_book', [
            'as' => 'address_book', 'uses' => 'AddressBookController@index'
        ]);

        Route::get('/address_book_create', [
            'as' => 'address_book_create', 'uses' => 'AddressBookController@create'
        ]);

        Route::post('/address_book_store', [
            'as' => 'address_book_store', 'uses' => 'AddressBookController@store'
        ]);

        Route::get('/address_book_show/{id}', [
            'as' => 'address_book_show', 'uses' => 'AddressBookController@show'
        ]);

        Route::get('/address_book_edit/{id}', [
            'as' => 'address_book_edit', 'uses' => 'AddressBookController@edit'
        ]);

        Route::post('/address_book_update', [
            'as' => 'address_book_update', 'uses' => 'AddressBookController@update'
        ]);

        Route::get('/address_book_destroy/{id}', [
            'as' => 'address_book_destroy', 'uses' => 'AddressBookController@destroy'
        ]);
    });
});

//==========================Instantiating Dingo Class===============================//
$api = app('Dingo\Api\Routing\Router');

//==========================Unprotected Routes===============================//
$api->version('v1', ['namespace' => 'App\Http\Controllers\Api\V1'], function ($api) {
    //==========================Rate Card Route==========================//
    $api->get('/api_address_book', array('as'=>'api-address_book', 'uses' => 'ApiAddressBookController@index'));
    $api->post('/api_address_book_store', array('as'=>'api-address_book_store', 'uses' => 'ApiAddressBookController@store'));
    $api->post('/api_address_book_showedit', array('as'=>'api-address_book_showedit', 'uses' => 'ApiAddressBookController@showedit'));
    $api->post('/api_address_book_update', array('as'=>'api-address_book_update', 'uses' => 'ApiAddressBookController@update'));
    $api->post('/api_address_book_destroy', array('as'=>'api-address_book_destroy', 'uses' => 'ApiAddressBookController@destroy'));

});
//==========================================================================//
