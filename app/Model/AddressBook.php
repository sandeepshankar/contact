<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AddressBook extends Model
{
    protected $table = 'address_book';
    protected $primaryKey = 'address_book_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address_book_id','user_id', 'contact_person_name','contact_person_number','address_line_1_to' ,'address_line_2_to','address_line_3_to',
        'pincode_to', 'city_to', 'state_to', 'country_to','address_line_1_from','address_line_2_from','address_line_3_from',
        'pincode_from', 'city_from', 'state_from', 'country_from',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function address_book_details()
    {
        return $this->hasMany('App\Model\AddressBookDetails', 'address_book_id', 'address_book_id');
    }
}
