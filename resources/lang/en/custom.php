<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Language Lines
    |--------------------------------------------------------------------------
    */



// Manage Address Book
    'contact_add_success' => 'Contact added Successfully.',
    'contact_add_error' => 'Unable To Add Contact.',
    'contact_update_success' => 'Contact updated Successfully.',
    'contact_update_error' => 'Unable To Update Contact.',
    'contact_delete_success' => 'Contact deleted Successfully.',
    'contact_delete_error' => 'Unable To Delete Contact.',

    'success' => 'Request Successful',
    'db_error' => 'DB error contact admin',




];
