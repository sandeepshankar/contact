@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <a class="btn btn-sm btn-primary" href="">Manage Profile</a>
                    <a class="btn btn-sm btn-primary" href="{{ url('/address_book') }}">Manage Address Book</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
