@extends('layouts.app')

@section('content')
    <div class="top-content">
        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Add Contact</div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/address_book_store') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('contact_person_name') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Contact Name</label>

                                        <div class="col-md-6">
                                            <input id="contact_person_name" type="text" class="form-control" name="contact_person_name"
                                                   value="{{ old('contact_person_name') }}">

                                            @if ($errors->has('contact_person_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('contact_person_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('contact_person_number') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Contact Number</label>

                                        <div class="col-md-6">
                                            <input id="contact_person_number" type="text" class="form-control" name="contact_person_number"
                                                   value="{{ old('contact_person_number') }}">

                                            @if ($errors->has('contact_person_number'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('contact_person_number') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <hr>
                                    <h5>From Address</h5>
                                    <div class="form-group{{ $errors->has('address_line_1_from') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Address Line 1</label>

                                        <div class="col-md-6">
                                            <input id="address_line_1_from" type="text" class="form-control" name="address_line_1_from"
                                                   value="{{ old('address_line_1_from') }}">

                                            @if ($errors->has('address_line_1_from'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('address_line_1_from') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('address_line_2_from') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Address Line 2</label>

                                        <div class="col-md-6">
                                            <input id="address_line_2_from" type="text" class="form-control" name="address_line_2_from"
                                                   value="{{ old('address_line_2_from') }}">

                                            @if ($errors->has('address_line_2_from'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('address_line_2_from') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('address_line_3_from') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Address Line 3</label>

                                        <div class="col-md-6">
                                            <input id="address_line_3_from" type="text" class="form-control" name="address_line_3_from"
                                                   value="{{ old('address_line_3_from') }}">

                                            @if ($errors->has('address_line_3_from'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('address_line_3_from') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('pincode_from') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Pincode</label>

                                        <div class="col-md-6">
                                            <input id="pincode_from" type="text" class="form-control" name="pincode_from"
                                                   value="{{ old('pincode_from') }}">

                                            @if ($errors->has('pincode_from'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('pincode_from') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('city_from') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">City</label>

                                        <div class="col-md-6">
                                            <input id="city" type="text" class="form-control" name="city_from"
                                                   value="{{ old('city_from') }}">

                                            @if ($errors->has('city_from'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('city_from') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('state_from') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">State</label>

                                        <div class="col-md-6">
                                            <input id="stat_frome" type="text" class="form-control" name="state_from"
                                                   value="{{ old('state_from') }}">

                                            @if ($errors->has('state_from'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('state_from') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('country_from') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Country</label>

                                        <div class="col-md-6">
                                            <input id="country_from" type="text" class="form-control" name="country_from"
                                                   value="{{ old('country_from') }}">

                                            @if ($errors->has('country_from'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('country_from') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>
                                    <hr>
                                    <h5>To Address</h5>

                                    <div class="form-group{{ $errors->has('address_line_1_to') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Address Line 1</label>

                                        <div class="col-md-6">
                                            <input id="address_line_1_to" type="text" class="form-control" name="address_line_1_to"
                                                   value="{{ old('address_line_1_to') }}">

                                            @if ($errors->has('address_line_1_to'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('address_line_1_to') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('address_line_2_to') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Address Line 2</label>

                                        <div class="col-md-6">
                                            <input id="address_line_2_to" type="text" class="form-control" name="address_line_2_to"
                                                   value="{{ old('address_line_2_to') }}">

                                            @if ($errors->has('address_line_2_to'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('address_line_2_to') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('address_line_3_to') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Address Line 3</label>

                                        <div class="col-md-6">
                                            <input id="address_line_3_to" type="text" class="form-control" name="address_line_3_to"
                                                   value="{{ old('address_line_3_to') }}">

                                            @if ($errors->has('address_line_3_to'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('address_line_3_to') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('pincode_to') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Pincode</label>

                                        <div class="col-md-6">
                                            <input id="pincode_to" type="text" class="form-control" name="pincode_to"
                                                   value="{{ old('pincode_to') }}">

                                            @if ($errors->has('pincode_to'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('pincode_to') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('city_to') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">City</label>

                                        <div class="col-md-6">
                                            <input id="city_to" type="text" class="form-control" name="city_to"
                                                   value="{{ old('city_to') }}">

                                            @if ($errors->has('city_to'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('city_to') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('state_to') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">State</label>

                                        <div class="col-md-6">
                                            <input id="state_to" type="text" class="form-control" name="state_to"
                                                   value="{{ old('state_to') }}">

                                            @if ($errors->has('state_to'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('state_to') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('country_to') ? ' has-error' : '' }}">
                                        <label for="" class="col-md-4 control-label">Country</label>

                                        <div class="col-md-6">
                                            <input id="country_to" type="text" class="form-control" name="country_to"
                                                   value="{{ old('country_to') }}">

                                            @if ($errors->has('country_to'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('country_to') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-sign-in"></i> Submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
