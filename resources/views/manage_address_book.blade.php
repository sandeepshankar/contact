@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Manage Address Book</h2>
                <hr>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Address Book List</strong></h3>
                    </div>
                    @if(Session::has('success'))
                        <div class="alert alert-success" id="success-alert">
                            <strong>Success!  </strong>{{ Session::get('success') }}test
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger" id="success-alert">
                            <strong>Error!  </strong>{{ Session::get('error') }}error
                        </div>
                    @endif
                    <div class="panel-body">
                        <a href="{{ url('/address_book_create') }}" class="btn btn-sm btn-primary">Add Contact</a>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                @if(!$data->isEmpty())
                                    <thead>
                                    <tr>
                                        <td class="text-center" style="width: 200px;"><strong>Contact Name</strong>
                                        </td>
                                        <td class="text-center">
                                            <strong>
                                                Contact Number
                                            </strong>
                                        </td>
                                        <td class="text-center" style="width: 100px;">
                                            <strong>
                                                Options
                                            </strong>
                                        </td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach ($data as $sub_data)

                                        <tr class="">
                                            <td class="hidden" id="address_book_id">{{ $sub_data->address_book_id }}</td>
                                            <td class="text-center">{{ $sub_data->contact_person_name }}</td>
                                            <td class="text-center">{{ $sub_data->contact_person_number }}</td>
                                            <td class="text-center" >
                                                <a style="color: #000000 !important;" href="{{ url('/address_book_edit/'.$sub_data->address_book_id) }}" ><span class="fa fa-edit" aria-hidden="true"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a style="color: #000000 !important;" href="{{ url('/address_book_show/'.$sub_data->address_book_id) }}" ><span class="fa fa-eye" aria-hidden="true"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a onclick="return confirm('Are you sure you want to delete the record ?')" style="color: #000000 !important;" href="{{ url('/address_book_destroy/'.$sub_data->address_book_id) }}" ><span class="fa fa-close" aria-hidden="true"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                @else
                                    <h3 class="text-center">No Addresses Added</h3>
                                @endif
                            </table>
                            {!! $data->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection