@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="invoice-title">
                    <h2>Contact Details</h2>
                </div>
                <a href="{{ url('/address_book') }}" class="btn btn-sm btn-primary">Manage Address Book</a>

                <hr>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Contact Details</div>
                    <div class="panel-body">
                        <div class="row table-responsive">

                            <table class="table table-striped">
                                <tr>
                                    <td>
                                        Contact Person Name :<br>
                                    </td>
                                    <td>
                                        {{ $data->contact_person_name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contact Person Number :
                                    </td>
                                    <td>
                                        {{ $data->contact_person_number }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        From Address :
                                    </td>
                                    <td>
                                        {{ $data->address_line_1_from." ,".$data->address_line_2_from." ,".$data->address_line_3_from." ,".$data->city_from." ,".$data->pincode_from." ,".$data->state_from." ,".$data->country_from }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        To Address :
                                    </td>
                                    <td>
                                        {{ $data->address_line_1_to." ,".$data->address_line_2_to." ,".$data->address_line_3_to." ,".$data->city_to." ,".$data->pincode_to." ,".$data->state_to." ,".$data->country_to }}                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>

@endsection